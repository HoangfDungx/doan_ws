;; Auto-generated. Do not edit!


(when (boundp 'robot_msgs::WheelSpeed)
  (if (not (find-package "ROBOT_MSGS"))
    (make-package "ROBOT_MSGS"))
  (shadow 'WheelSpeed (find-package "ROBOT_MSGS")))
(unless (find-package "ROBOT_MSGS::WHEELSPEED")
  (make-package "ROBOT_MSGS::WHEELSPEED"))

(in-package "ROS")
;;//! \htmlinclude WheelSpeed.msg.html


(defclass robot_msgs::WheelSpeed
  :super ros::object
  :slots (_l _r ))

(defmethod robot_msgs::WheelSpeed
  (:init
   (&key
    ((:l __l) 0.0)
    ((:r __r) 0.0)
    )
   (send-super :init)
   (setq _l (float __l))
   (setq _r (float __r))
   self)
  (:l
   (&optional __l)
   (if __l (setq _l __l)) _l)
  (:r
   (&optional __r)
   (if __r (setq _r __r)) _r)
  (:serialization-length
   ()
   (+
    ;; float64 _l
    8
    ;; float64 _r
    8
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; float64 _l
       (sys::poke _l (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;; float64 _r
       (sys::poke _r (send s :buffer) (send s :count) :double) (incf (stream-count s) 8)
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; float64 _l
     (setq _l (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;; float64 _r
     (setq _r (sys::peek buf ptr- :double)) (incf ptr- 8)
   ;;
   self)
  )

(setf (get robot_msgs::WheelSpeed :md5sum-) "9df82ee6d637c8a14512bbf523890470")
(setf (get robot_msgs::WheelSpeed :datatype-) "robot_msgs/WheelSpeed")
(setf (get robot_msgs::WheelSpeed :definition-)
      "float64 l
float64 r
")



(provide :robot_msgs/WheelSpeed "9df82ee6d637c8a14512bbf523890470")


