# CMake generated Testfile for 
# Source directory: /home/hoangdung/doan_ws/src
# Build directory: /home/hoangdung/doan_ws/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs("gtest")
subdirs("robot_description")
subdirs("robot_msgs")
subdirs("robot_kinematics")
subdirs("robot_bringup")
