#ifndef ROBOT_STATE_H_
#define ROBOT_STATE_H_

#include "ros/ros.h"
#include <geometry_msgs/Pose.h>
#include <geometry_msgs/Twist.h>
#include <robot_msgs/WheelSpeed.h>

#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

class RobotState
{
private:
    ros::NodeHandle nh_;

public:
    double time_frame = -1, dt = 0;
    double r, L;
    double x = 0, y = 0, theta = 0;
    // double vx, v_theta;
    geometry_msgs::Twist vel;
    // double omega_l, omega_r;
    robot_msgs::WheelSpeed w;

    RobotState(ros::NodeHandle& nh);
    ~RobotState();

    void setWheelSpeed(const robot_msgs::WheelSpeed& _w);
    void setVel(const geometry_msgs::Twist& _vel);

    robot_msgs::WheelSpeed getWheelSpeed();
    geometry_msgs::Twist getVel();
    geometry_msgs::Pose getRobotPose();
};

RobotState::RobotState(ros::NodeHandle& nh) : nh_(nh)
{
    nh_.getParam(ros::this_node::getName() + "/wheel_radius", r);
    nh_.getParam(ros::this_node::getName() + "/wheel_distance", L);
}

RobotState::~RobotState()
{
}

void RobotState::setWheelSpeed(const robot_msgs::WheelSpeed& _w)
{
    w = _w;

    // Convert to robot velocity
    vel.linear.x = r * (w.l + w.r) / 2;
    vel.linear.y = 0.0;
    vel.linear.z = 0.0;

    vel.angular.x = 0;
    vel.angular.y = 0;
    vel.angular.z = r * (w.r - w.l) / L;

    double now = ros::Time::now().toSec();
    // Integrate Robot pose
    if (time_frame < 0) {
        time_frame = now;
        return;
    }

    dt = now - time_frame;
    time_frame = now;

    x += vel.linear.x * cos(theta) * dt;
    y += vel.linear.x * sin(theta) * dt;
    theta += vel.angular.z * dt;
}

void RobotState::setVel(const geometry_msgs::Twist& _vel)
{
    vel = _vel;

    // Convert to wheel speed
    w.r = (2 * vel.linear.x + vel.angular.z * L) / (2 * r);
    w.l = (2 * vel.linear.x - vel.angular.z * L) / (2 * r);
}

robot_msgs::WheelSpeed RobotState::getWheelSpeed()
{
    return w;
}

geometry_msgs::Twist RobotState::getVel()
{
    return vel;
}
    
geometry_msgs::Pose RobotState::getRobotPose()
{
    // Convert x, y, theta -> geometry_msgs Pose
    geometry_msgs::Pose pose;
    pose.position.x = x;
    pose.position.y = y;
    pose.position.z = 0.0;

    // convert theta to quaternion
    tf2::Quaternion quad;
    quad.setRPY(0, 0, theta);
    pose.orientation = tf2::toMsg(quad);

    return pose;
}

#endif
