#ifndef ROBOT_KINEMATICS_NODE_H_
#define ROBOT_KINEMATICS_NODE_H_

#include "ros/ros.h"
#include "robot_kinematics/robot_state.h"
#include <nav_msgs/Odometry.h>

#include <tf2_ros/transform_broadcaster.h>
#include <geometry_msgs/TransformStamped.h>

class RobotKinematicsNode
{
private:
    ros::NodeHandle nh_;
    RobotState robot_state_;

    std::vector<double> pose_cov = std::vector<double>(36, 0);
    std::vector<double> vel_cov = std::vector<double>(36, 0);

    ros::Publisher wheel_speed_cmd_publisher_;
    ros::Publisher odom_publisher_;
    ros::Subscriber wheel_speed_subscriber_;
    ros::Subscriber cmd_vel_subscriber_;

    tf2_ros::TransformBroadcaster br;

public:
    RobotKinematicsNode(ros::NodeHandle nh);
    ~RobotKinematicsNode();

    void publishOdometry();
    void wheelSpeedCallback(const robot_msgs::WheelSpeed::ConstPtr& _w);
    void cmdVelCallback(const geometry_msgs::Twist& _vel);
};

RobotKinematicsNode::RobotKinematicsNode(ros::NodeHandle nh) 
    : nh_(nh), robot_state_(nh_)
{
    wheel_speed_cmd_publisher_ = nh_.advertise<robot_msgs::WheelSpeed>("wheel_speed/cmd", 1);
    odom_publisher_ = nh_.advertise<nav_msgs::Odometry>("odom", 1);
    wheel_speed_subscriber_ = nh_.subscribe("wheel_speed/real", 1, &RobotKinematicsNode::wheelSpeedCallback, this);
    cmd_vel_subscriber_ = nh_.subscribe("cmd_vel", 1, &RobotKinematicsNode::cmdVelCallback, this);
}

RobotKinematicsNode::~RobotKinematicsNode()
{
}

void RobotKinematicsNode::publishOdometry()
{
    nav_msgs::Odometry odom;
    odom.header.stamp = ros::Time::now();
    odom.header.frame_id = "odom";
    odom.child_frame_id = "base_footprint";

    odom.pose.pose = robot_state_.getRobotPose();
    odom.twist.twist = robot_state_.getVel();

    vel_cov[0] = robot_state_.vel.linear.x * 0.01 + 0.01;
    vel_cov[35] = robot_state_.vel.angular.z * 0.01 + 0.01;

    pose_cov[0] += vel_cov[0] * cos(robot_state_.theta) * robot_state_.dt;
    pose_cov[7] += vel_cov[0] * sin(robot_state_.theta) * robot_state_.dt;
    pose_cov[35] += vel_cov[35]  * robot_state_.dt;

    for (int i = 0; i < pose_cov.size(); ++i) {
        odom.pose.covariance[i] = pose_cov[i];
        odom.twist.covariance[i] = vel_cov[i];
    }

    odom_publisher_.publish(odom);
    geometry_msgs::TransformStamped transformStamped;
    transformStamped.header = odom.header;
    transformStamped.child_frame_id = odom.child_frame_id;

    transformStamped.transform.translation.x = odom.pose.pose.position.x;
    transformStamped.transform.translation.y = odom.pose.pose.position.y;
    transformStamped.transform.translation.z = odom.pose.pose.position.z;
    transformStamped.transform.rotation = odom.pose.pose.orientation;
    br.sendTransform(transformStamped);
}

void RobotKinematicsNode::wheelSpeedCallback(const robot_msgs::WheelSpeed::ConstPtr& _w)
{
    robot_state_.setWheelSpeed(*_w);
    publishOdometry();
}

void RobotKinematicsNode::cmdVelCallback(const geometry_msgs::Twist& _vel)
{
    robot_state_.setVel(_vel);
    wheel_speed_cmd_publisher_.publish(robot_state_.getWheelSpeed());
}

#endif
