#include "ros/ros.h"
#include "robot_kinematics/robot_kinematics_node.h"

int main(int argc, char** argv)
{
    ros::init(argc, argv, "robot_kinematics_node");
    ros::NodeHandle nh;
    
    RobotKinematicsNode node(nh);
    ros::spin();

    return 1;
}